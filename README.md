# 可视化技能编辑器 :neckbeard:  :smiling_imp: 

#### 介绍

 **可视化技能编辑器目前全部的开发工作都在这个Moba项目，大家有需要的可以看看。[Moba项目链接](https://gitee.com/NKG_admin/NKGMobaBasedOnET)** 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0503/203823_9f8ff726_2253805.png "屏幕截图.png")

 **整合GitHub开源项目Node_Editor和Odin的可视化技能编辑器** 

 **Node_Editor地址：[https://github.com/Seneral/Node_Editor_Framework.git](https://github.com/Seneral/Node_Editor_Framework.git)** 

 **Odin官网：[http://sirenix.net/](http://sirenix.net/)** 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0520/141256_9db78fd1_2253805.jpeg "截图")