//------------------------------------------------------------
// Author: 烟雨迷离半世殇
// Mail: 1778139321@qq.com
// Data: 2019年5月17日 21:03:43
//------------------------------------------------------------

using Sirenix.OdinInspector;

namespace SkillDemo
{
    public class NodeDataForSkillBuff : BaseNodeData
    {
        [HideLabel] [Title("技能Buff模块")] public SkillBuffBase SkillBuffBases;
    }
}